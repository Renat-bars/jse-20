package ru.tsc.almukhametov.tm.api;

import ru.tsc.almukhametov.tm.model.AbstractEntity;

import java.util.Comparator;
import java.util.List;

public interface IRepository<E extends AbstractEntity> {

    E add(E entity);

    void remove(E entity);

    List<E> findAll();

    List<E> findAll(Comparator<E> comparator);

    void clear();

    E findById(final String id);

    E findByIndex(final Integer index);

    E removeById(final String id);

    E removeByIndex(final Integer index);

    boolean existById(final String id);

    boolean existByIndex(final int index);

    Integer getSize();

}
