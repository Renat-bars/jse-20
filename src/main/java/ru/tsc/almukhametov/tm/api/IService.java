package ru.tsc.almukhametov.tm.api;

import ru.tsc.almukhametov.tm.model.AbstractEntity;

public interface IService<E extends AbstractEntity> extends IRepository<E> {
}
