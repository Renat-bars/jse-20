package ru.tsc.almukhametov.tm.api.entity;

public interface IWBS extends IHasCreated, IHasName, IHasStartDate, IHasStatus {
}
