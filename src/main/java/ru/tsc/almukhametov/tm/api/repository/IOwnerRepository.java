package ru.tsc.almukhametov.tm.api.repository;

import ru.tsc.almukhametov.tm.api.IRepository;
import ru.tsc.almukhametov.tm.model.AbstractOwnerEntity;

import java.util.Comparator;
import java.util.List;

public interface IOwnerRepository<E extends AbstractOwnerEntity> extends IRepository<E> {

    E add(String userId, E entity);

    void remove(String userId, E entity);

    List<E> findAll(String userId);

    List<E> findAll(String userId, Comparator<E> comparator);

    void clear(String userId);

    E findById(String userId, final String id);

    E findByIndex(String userId, final Integer index);

    E removeById(String userId, final String id);

    E removeByIndex(String userId, final Integer index);

    boolean existById(String userId, final String id);

    boolean existByIndex(String userId, final int index);

}
