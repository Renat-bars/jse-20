package ru.tsc.almukhametov.tm.api.service;

import ru.tsc.almukhametov.tm.enumerated.Status;
import ru.tsc.almukhametov.tm.model.Task;

public interface ITaskService extends IOwnerService<Task> {

    void create(final String userId, final String name, final String description);

    Task findByName(final String userId, final String name);

    Task removeByName(final String userId, final String name);

    Task updateById(final String userId, final String id, final String name, final String description);

    Task updateByIndex(final String userId, final Integer index, final String name, final String description);

    Task startById(final String userId, final String id);

    Task startByIndex(final String userId, final Integer id);

    Task startByName(final String userId, final String name);

    Task finishById(final String userId, final String id);

    Task finishByIndex(final String userId, final Integer index);

    Task finishByName(final String userId, final String name);

    Task changeTaskStatusById(final String userId, final String id, final Status status);

    Task changeTaskStatusByIndex(final String userId, final Integer index, final Status status);

    Task changeTaskStatusByName(final String userId, final String name, final Status status);

}
