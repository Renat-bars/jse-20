package ru.tsc.almukhametov.tm.api.service;

import ru.tsc.almukhametov.tm.enumerated.Status;
import ru.tsc.almukhametov.tm.model.Project;

public interface IProjectService extends IOwnerService<Project> {

    void create(final String userId, final String name, final String description);

    Project findByName(final String userId, final String name);

    Project removeByName(final String userId, final String name);

    Project updateById(final String userId, final String id, final String name, final String description);

    Project updateByIndex(final String userId, final Integer index, final String name, final String description);

    Project startById(final String userId, final String id);

    Project startByIndex(final String userId, final Integer index);

    Project startByName(final String userId, final String name);

    Project finishById(final String userId, final String id);

    Project finishByIndex(final String userId, final Integer index);

    Project finishByName(final String userId, final String name);

    Project changeProjectStatusById(final String userId, final String id, final Status status);

    Project changeProjectStatusByIndex(final String userId, final Integer index, final Status status);

    Project changeProjectStatusByName(final String userId, final String name, final Status status);

    Project setStatus(Status status);

}
