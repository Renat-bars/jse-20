package ru.tsc.almukhametov.tm.repository;

import ru.tsc.almukhametov.tm.api.repository.IOwnerRepository;
import ru.tsc.almukhametov.tm.model.AbstractOwnerEntity;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractOwnerRepository<E extends AbstractOwnerEntity> extends AbstractRepository<E>
        implements IOwnerRepository<E> {

    public E add(final String userId, final E entity) {
        entity.setUserId(userId);
        list.add(entity);
        return entity;
    }

    public void remove(final String userId, final E entity) {
        if (!userId.equals(entity.getUserId())) return;
        list.remove(entity);
    }

    public List<E> findAll(final String userId) {
        final List<E> result = new ArrayList<>();
        for (E entity : list) {
            if (userId.equals(entity.getUserId())) result.add(entity);
        }
        return result;
    }

    public List<E> findAll(final String userId, final Comparator<E> comparator) {
        final List<E> entities = new ArrayList<>(this.list);
        entities.sort(comparator);
        return entities;
    }

    public void clear(final String userId) {
        final List<E> list = findAll(userId);
        this.list.removeAll(list);
    }

    public E findById(final String userId, final String id) {
        for (E entity : list) {
            if (!userId.equals(entity.getUserId())) continue;
            if (id.equals(entity.getId())) return entity;
        }
        return null;
    }

    public E findByIndex(final String userId, final Integer index) {
        final List<E> list = findAll(userId);
        return list.get(index);
    }

    public E removeById(final String userId, final String id) {
        final E entity = findById(userId, id);
        if (entity == null) return null;
        list.remove(entity);
        return entity;
    }

    public E removeByIndex(final String userId, final Integer index) {
        final E entity = findByIndex(userId, index);
        if (entity == null) return null;
        list.remove(entity);
        return entity;
    }

    public boolean existById(final String userId, final String id) {
        final E entity = findById(userId, id);
        return entity != null;
    }

    public boolean existByIndex(final String userId, final int index) {
        final List<E> list = findAll(userId);
        if (index < 0) return false;
        return index < list.size();
    }


}
