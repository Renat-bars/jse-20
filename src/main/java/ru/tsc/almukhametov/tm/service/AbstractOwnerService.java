package ru.tsc.almukhametov.tm.service;

import ru.tsc.almukhametov.tm.api.repository.IOwnerRepository;
import ru.tsc.almukhametov.tm.api.service.IOwnerService;
import ru.tsc.almukhametov.tm.exception.empty.EmptyIdException;
import ru.tsc.almukhametov.tm.exception.empty.EmptyIndexException;
import ru.tsc.almukhametov.tm.exception.entity.EntityNotFoundException;
import ru.tsc.almukhametov.tm.model.AbstractOwnerEntity;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractOwnerService<E extends AbstractOwnerEntity> extends AbstractService<E>
        implements IOwnerService<E> {

    private final IOwnerRepository<E> repository;

    public AbstractOwnerService(IOwnerRepository<E> repository) {
        super(repository);
        this.repository = repository;
    }

    @Override
    public E add(final String userId, final E entity) {
        if (entity == null) throw new EntityNotFoundException();
        return repository.add(userId, entity);
    }

    @Override
    public void remove(final String userId, E entity) {
        if (entity == null) throw new EntityNotFoundException();
        repository.remove(userId, entity);
    }

    @Override
    public List<E> findAll(final String userId) {
        return repository.findAll(userId);
    }

    @Override
    public List<E> findAll(final String userId, Comparator<E> comparator) {
        if (comparator == null) return Collections.emptyList();
        return repository.findAll(userId, comparator);
    }

    @Override
    public void clear(final String userId) {
        repository.clear(userId);
    }

    @Override
    public E findById(final String userId, final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return repository.findById(userId, id);
    }

    @Override
    public E findByIndex(final String userId, final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        if (repository.getSize() < index - 1) throw new EntityNotFoundException();
        return repository.findByIndex(userId, index);
    }

    @Override
    public E removeById(final String userId, final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return repository.removeById(userId, id);
    }

    @Override
    public E removeByIndex(final String userId, final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        if (repository.getSize() < index - 1) throw new EntityNotFoundException();
        return repository.removeByIndex(userId, index);
    }


    @Override
    public boolean existById(final String userId, final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return repository.existById(userId, id);
    }

    @Override
    public boolean existByIndex(final String userId, final int index) {
        return repository.existByIndex(userId, index);
    }

}
