package ru.tsc.almukhametov.tm.command.project;

import ru.tsc.almukhametov.tm.command.AbstractProjectCommand;
import ru.tsc.almukhametov.tm.constant.SystemDescriptionConst;
import ru.tsc.almukhametov.tm.constant.TerminalConst;
import ru.tsc.almukhametov.tm.exception.empty.EmptyIndexException;
import ru.tsc.almukhametov.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.almukhametov.tm.model.Project;
import ru.tsc.almukhametov.tm.util.TerminalUtil;

public final class ProjectUpdateByIndexCommand extends AbstractProjectCommand {

    @Override
    public String name() {
        return TerminalConst.PROJECT_UPDATE_BY_INDEX;
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return SystemDescriptionConst.PROJECT_UPDATE_BY_INDEX;
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthenticationService().getUserId();
        System.out.println("Enter Index");
        final Integer index = TerminalUtil.nextNumber() - 1;
        if (!serviceLocator.getProjectService().existByIndex(userId, index)) throw new EmptyIndexException();
        System.out.println("Enter Name");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter description");
        final String description = TerminalUtil.nextLine();
        final Project projectUpdate = serviceLocator.getProjectService().updateByIndex(userId, index, name, description);
        if (projectUpdate == null) throw new ProjectNotFoundException();
    }

}
