package ru.tsc.almukhametov.tm.command.project;

import ru.tsc.almukhametov.tm.command.AbstractProjectCommand;
import ru.tsc.almukhametov.tm.constant.SystemDescriptionConst;
import ru.tsc.almukhametov.tm.constant.TerminalConst;

public final class ProjectClearCommand extends AbstractProjectCommand {

    @Override
    public String name() {
        return TerminalConst.PROJECT_CLEAR;
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return SystemDescriptionConst.PROJECT_CLEAR;
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthenticationService().getUserId();
        System.out.println("[ClEAR PROJECTS]");
        serviceLocator.getProjectService().clear(userId);
        System.out.println("[SUCCESS CLEAR]");
    }

}
