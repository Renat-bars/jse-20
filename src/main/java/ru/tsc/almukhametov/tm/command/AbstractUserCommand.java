package ru.tsc.almukhametov.tm.command;

import ru.tsc.almukhametov.tm.exception.system.UserNotFoundException;
import ru.tsc.almukhametov.tm.model.User;

public abstract class AbstractUserCommand extends AbstractCommand {

    protected void showUser(final User user) {
        if (user == null) throw new UserNotFoundException();
        System.out.println("[USER PROFILE]");
        System.out.println("ID: " + user.getId());
        System.out.println("LOGIN: " + user.getLogin());
        if (user.getLastName() != null && !user.getLastName().isEmpty())
            System.out.println("NAME: " + user.getLastName() + " " + user.getFirstName() + " " + user.getMiddleName());
        if (user.getEmail() != null && user.getEmail().isEmpty())
            System.out.println("EMAIL: " + user.getEmail());
        System.out.println("ROLE: " + user.getRole().getDisplayName());

    }

}
