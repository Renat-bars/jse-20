package ru.tsc.almukhametov.tm.exception.entity;

import ru.tsc.almukhametov.tm.exception.AbstractException;

public class EntityNotFoundException extends AbstractException {

    public EntityNotFoundException() {
        super("Error. Project not found.");
    }
}
